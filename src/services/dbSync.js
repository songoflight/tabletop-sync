// (function() {
  window.db = {
    prep: function() {
      if (db.local === undefined || db.remote === undefined) {  
        console.info('preping DB')
        db.local = new PouchDB('local', {auto_compaction: true});
        db.remote = new PouchDB('http://ardentgrace.com:5984/tabletop', {ajax: {cache: false,timeout: 10000,},auth: {username: 'browsermonkey',password: 'j4HEQ9jtP9CjyWTmCZIr3LGmQ'}});
        // db.remote = 'http://browsermonkey:j4HEQ9jtP9CjyWTmCZIr3LGmQ@ardentgrace.com:5984/tabletop';
        db.local.sync(db.remote, {live:true, retry:true})
        .on('complete', db.syncComp)
        .on('change', db.syncChange)
        .on('error', db.syncError);
      }
    },
    syncComp: function(complete) {
      console.info('sync complete');
      console.log(complete);
    },
    syncChange: function(change) {
      console.info('sync change');
      console.log(change);
    },
    syncError: function(error) {
      console.info('sync error');
      console.log(error);
    }
  }
// })();