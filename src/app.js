import {dbSync} from 'src/services/dbSync.js';
import {session} from 'src/services/session.js';
import {randoms} from 'src/services/randoms.js';
// @inject(dbSync)
export class App {
  configureRouter(config, router) {
    config.title = 'TableTop Sync';
    config.options.pushState = true;
    config.options.root = '/';
    config.map([
      { route: ['', 'welcome'], name: 'welcome',      moduleId: './pages/welcome/welcome',      nav: false, title: 'Welcome' },
      { route: 'session',         name: 'session',        moduleId: './pages/session/session',        nav: true, title: 'Session' },
      { route: 'characters',         name: 'characters',        moduleId: './pages/characters/characters',        nav: true, title: 'Characters' },
      { route: 'search',         name: 'search',        moduleId: './pages/search/search',        nav: true, title: 'Search' }
    ]);

    this.router = router;
  }
}