export class Info {
	heading = 'Information Lookup';
	suspects = [];
	units = [];
	displayLimit = 10;
	searchMatches = 0;
	search = '';
	filter = {
		skills: true,
		assets: true,
		complications: true,
		items: true,
		weapons: true,
		npcs: true,
	}

  init() {
    db.prep();
    this.getSuspects();
  };

  getSuspects() {
    return db.local.get('serenity')
    .then(msg => {this.suspects = msg;});
  };

	searchGo() {
		if (this.search.length > 0) {
			this.units = [];
			this.searchMatches = 0;
		/* ASSETS */
			var searchKeys = [];
			searchKeys = searchKeys.concat(this.suspects.assets);
			searchKeys = searchKeys.concat(this.suspects.complications);
			searchKeys = searchKeys.concat(this.suspects.skills);
			searchKeys = searchKeys.concat(this.suspects.items);
			searchKeys.forEach(item => {
				if (item.title.toLowerCase().search(this.search.toLowerCase()) > -1
				|| item.description.toLowerCase().search(this.search.toLowerCase()) > -1) {
					if (this.units.length < this.displayLimit) {
						this.units.push(item);
					}
					this.searchMatches++;
				}
			});
		}
		else {
			this.searchClear();
		}
	};

	searchClear() {
		this.units = [];
		this.searchMatches = 0;
		this.search = '';		
	}
}