//import {computedFrom} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
// import {dbSync} from 'src/services/dbSync.js';
// import {HttpClient} from 'aurelia-fetch-client';
// import 'fetch';
// @inject(HttpClient)

export class Characters {
  heading = 'Character Management';
  subHeading = 'Summary';
  activeSection = 'summary';
  // playerID = session.playerID;
  activeIcon = "fa-info-circle";
  saveIcon = 'fa-cloud-upload';
  saveText = 'Savea';
  saveClass = 'danger';
  contentClass = '';
  popoverClass = 'hidden';
  popoverBind = [];
  popoverKeys = [];
  popoverAction = '';
  popoverTitle = '';
  popoverIndex = 0;
  diceLevels = {
    stats: ["D2","D4","D6","D8","D10","D12","D12+D2","D12+D4"],
    skillGen: ["D2","D4","D6"],
    skillSpec: ["","D8","D10","D12","D12+D2","D12+D4","D12+D6"]
  }
  charData = [];
  playerData = [];
  lists = {
    assets: [],
    complications: [],
    skills: [],
  };
  sections = [
    {
      title: "Summary",
      href: "/characters",
      section: "summary",
      icon: "fa-address-card",
      isActive: true
    },
    {
      title: "General",
      href: "/characters",
      section: "general",
      icon: "fa-info-circle",
      isActive: true
    },
    {
      title: "Status",
      href: "/characters",
      section: "status",
      icon: "fa-heartbeat",
      isActive: true
    },
    {
      title: "Skills",
      href: "/characters",
      section: "skills",
      icon: "fa-flask",
      isActive: true
    },
    {
      title: "Inventory",
      href: "/characters",
      section: "inventory",
      icon: "fa-shopping-bag",
      isActive: true
    },
  ];

  init() {
    this.session = session;
    db.prep();
    this.fetchPlayerData();
    this.prepDropDowns();
    if (window.session.playerID && window.session.playerID != null) {
      this.playerSelect(window.session.playerID);
    }
  };

  sectionSelect(section) {
    this.subHeading = section.title;
    this.activeIcon = section.icon;
    this.activeSection = section.section;
  };

  fetchCharData(key) {
  // Key corresponds to the associated accountID
    if (key && !isNaN(key)) {
      this.charData = [];
      db.local.get('player_'+key)
      .then(msg => {this.charData = msg;});
    }
  };

  fetchPlayerData() {
    db.local.get('accounts')
    .then(msg => {this.playerData = msg;});
  };

  updateDB() {
    this.saveClass = 'working';
    this.saveText = 'Saving ...';
    this.saveIcon = 'fa-cog fa-spin';
    console.log(this.charData);
    db.local.put(this.charData)
      .then(msg => {
        if (msg.ok === true) {
          this.saveClass = 'add';
          this.saveText = 'Success!';
          this.saveIcon = 'fa-circle-o-notch fa-spin';
        }
        else {
          this.saveClass = 'error';
          this.saveText = 'Save Failed!';
          this.saveIcon = 'fa-times-circle-o'; 
        }
      });
    var context = this;
    setTimeout(function() {
      context.saveIcon = 'fa-cloud-upload';
      context.saveText = 'Save';
      context.saveClass = 'danger';
      context.fetchCharData(window.session.playerID);
    }, 2500);
  }

  popoverShow(event, dataGroup) {
    var target = event.target;
    if (target.dataset.type === undefined) {
      target = target.parentNode;
    }
    this.contentClass = 'disabled';
    this.popoverClass = 'popoverShow';
    this.popoverAction = target.dataset.action;
    this.popoverKeys = this.keyModel[target.dataset.type];
    // this.popoverKeys = this.keyModel[target.dataset.type].split(',');
    this.popoverBind = dataGroup;
    console.log(this.popoverBind);
    // console.log(target);
    if (target.dataset.title !== undefined) {
      this.popoverTitle = target.dataset.title.toUpperCase();
    }
    if (target.dataset.index !== undefined) {
      this.popoverIndex = target.dataset.index;
    }
  };

  popoverProcess(context) {
    switch (context) {
      case 'add':
        var temp = {};
        var input = document.querySelectorAll('.popInputadd');
        for (var i = 0; i < this.popoverKeys.length; i++) {
          temp[this.popoverKeys[i].data] = input[i].value;
        }
        console.log(temp);
        this.popoverBind.push(temp);
      break;
      case 'edit':
        var input = document.querySelectorAll('.popInputedit');
        console.log(input);
        var target = this.popoverBind[this.popoverIndex];
        for (var i = 0; i < this.popoverKeys.length; i++) {
          var key = input[i].dataset.key;
          target[key] = input[i].value;
        }
      break;
      case 'delete':
        this.popoverBind.splice(this.popoverIndex, 1);
      break;
    }
    this.popoverHide();
  };

  popoverHide() {
    this.contentClass = '';
    this.popoverClass = 'hidden';
    this.popoverTitle = '';
    this.popoverAction = '';
    this.popoverContext = '';
    this.popoverKeys = [];
    this.popoverBind = [];
  };

  minorToggle(event, key) {
    if(event !== undefined) {
      var target = event.target;
      var value = event.target.value;
      if (value.toLowerCase() == 'minor') {
        target.value = "MAJOR";
        key.level = "MAJOR";
        target.classList.remove('minorInput');
        target.classList.add('majorInput');
      }
      else {
        target.value = "MINOR";
        key.level = "MINOR";
        target.classList.remove('majorInput');
        target.classList.add('minorInput');
      }
    }
  }

  prepDropDowns() {
    var lists = [{assets:[]}, {complications:[]}, {skills:[]}]
    db.local.get('serenity')
      .then(msg => {
        msg.assets.forEach(i => {this.lists.assets.push(i.title.split('(')[0])});
        msg.complications.forEach(i => {this.lists.complications.push(i.title.split('(')[0])});
        msg.skills.forEach(i => {this.lists.skills.push(i.title.split('(')[0])});
      })
      .then(msg => {
        this.keyModel = {
          asset: [
            {data: "title", label: "Asset", input: "select", options: this.lists.assets}, 
            {data: "level", label: "Level", input: "select", options: ["MINOR","MAJOR"]},
            {data: "note", label: "Note", input: "text", default: ""}
          ],
          complication: [
            {data: "title", label: "Complication", input: "select", options: this.lists.complications}, 
            {data: "level", label: "Level", input: "select", options: ["MINOR","MAJOR"]},
            {data: "note", label: "Note", input: "text", default: ""}
          ],
          skill: [
            {data: "title", label: "skill", input: "select", options: this.lists.skills},
            {data: "level", label: "General Level", input: "select", options: this.diceLevels.skillGen},
            {data: "spec", label: "Specialization", input: "text", default: ""},
            {data: "specLvl", label: "Spec. Level", input: "select", options: this.diceLevels.skillSpec}
          ],
          item: [
            {data: "title", label: "Item", input: "text", default: ""},
            {data: "description", label: "Description", input: "text", default: ""},
            {data: "qty", label: "Quantity", input: "text", default: "1"}
          ]
        }        
      })
  }

  playerClear() {
    window.session.playerID = null;
    this.playerID = null;
    this.charData = [];
  }

  playerSelect(key) {
    window.session.playerID = key;
    this.playerID = key;
    this.fetchCharData(key);
  }
}
