//import {computedFrom} from 'aurelia-framework';
// import {inject} from 'aurelia-framework';
// @inject(randoms);

export class Session {
  heading = 'Session Information';
  subHeading = 'Plot';
  activeSection = 'plot';
  activeIcon = 'fa-book';
  loadExcuse = randoms.genExcuse();
  sections = [
    {
      title: "Plot",
      href: "#/session/plot",
      section: "plot",
      icon: "fa-book",
      isActive: true
    },
    {
      title: "Combat",
      href: "#/session/combat",
      section: "combat",
      icon: "fa-exclamation-triangle",
      isActive: true
    },
    {
      title: "Experience",
      href: "#/session/experience",
      section: "experience",
      icon: "fa-graduation-cap",
      isActive: true
    }
  ];
  sectionSelect = function(section) {
    this.subHeading = section.title;
    this.activeIcon = section.icon;
    this.excuseCheck();
  };
  excuseCheck = function() {
      this.loadExcuse = randoms.genExcuse();
  };
}
  //Getters can't be directly observed, so they must be dirty checked.
  //However, if you tell Aurelia the dependencies, it no longer needs to dirty check the property.
  //To optimize by declaring the properties that this getter is computed from, uncomment the line below
  //as well as the corresponding import above.
  //@computedFrom('firstName', 'lastName')