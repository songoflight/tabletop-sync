import environment from './environment';
// import {dbSync} from 'src/services/dbSync.js';
// import 'bootstrap';

//Configure Bluebird Promises.
Promise.config({
  warnings: {
    wForgottenReturn: false
  }
});

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources');
  // aurelia.use.plugin('aurelia-animator-css');
  // aurelia.use.plugin('aurelia-html-import-template-loader');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
