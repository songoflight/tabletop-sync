
(function() {
	randoms = {
		init: function() {
			var target = document.querySelector('#loadingMsg');
			if (target != null) {
				randoms.loadingMessage(target);
				setInterval(function() {
					randoms.loadingMessage(target);
				}, 2500);
			}
		},
		loadingMessage: function(target) {
			var messages = [
				'Owning all the n00bs',
				'Coming back with a sandwich',
				'Opening the pod bay doors',
				'Teaching AI to become self-aware',
				'Checking on the zombie apocalypse',
				'Writing another Spider Man reboot',
				'Simply walking into Mordor',
				'Generating random loot',
				'Destroying all the Cat Memes',
				'Signing you up for more Spam',
				'Looking for the Mountain Dew... Its in the fridge',
				'Casting magic missile... at the darkness',
				'Making more interesting NPC\'s',
				'Generating epic loot',
				'Teaching goblins how to fire rockets',
				'Going home to rethink my life',
				'Trying to find the droids I was looking for',
				'Looking for signs of intelligent life',
				'Power-leveling the NPCs',
				'Making one website... to rule them all!',
				'If I’m not back in five minutes, just wait longer',
				'Strange things are afoot at the Circle K.',
				'This episode was BADLY written!',
				'Looking for great powers, to go with my great responsibilities',

				'Asking Admiral Ackbar if this is a trap'
			];
			target.innerHTML = messages[Math.floor(Math.random() * messages.length)];
		},
		genExcuse: function() {
			var message = '';
			var excuse = '';
			var preface = ['Because','There can be only one explanation:', 'This is what happens when', 'It all started when','[Insert Random Excuse]','All I know is that'];
			var exclaim = ['Yikes,','Wow,','Sorry,','My Bad!','Epic fail!','It\'s no good,','Apologies,','Error:','It\'s not my fault'];
			var msg = ["I couldn't load that",	"this is missing", "this is not finished", "it's missing", "it's not gonna work", "it's not responding", "they're all gone", "it can't be done", "it's taking too long"]; 
			var bridge = ["must have", 'recently', 'effortlessly', 'underhandedly', 'eventually', 'mistakenly', 'unintentionally', 'maliciously', 'ruefully', 'angrily', 'gleefully', 'easily', 'loudly', 'quietly', 'cleverly', 'foolishly', 'fiendishly'];
			var verb = ['stole', 'ate', 'absconded with', 'amused', 'misdirected', 'lied to', 'busted', 'destroyed', 'tainted', 'corrupted', 'painted', 'decorated', 're-engineered', 'incapacitated', 'defeated', 'jerry-rigged', 'lost', 'never found', 'brainwashed', 'misinterpreted', 'scared off', 'deleted', 'overtaxed', 'underestimated', 'infected', 'offended', 'hid', 'transformed', 'regurgitated'];
			var noun = ['The magic box', 'A hungry hungry hippo', 'The magical unicorn', 'An adorable little animal', 'The code', 'The data', 'A critical table', 'The backups', 'A very important fish', 'The magical unicorn', 'A required potato', 'A school of fish', 'My lunch', 'My heart', 'My breakfast', 'My dinner', 'A powerful enemy', 'My family', 'Your computer', 'Your browser', 'Your monitor', 'Your mouse', 'Your keyboard', "The Easter Bunny", "Santa Clause", "A gang of angry elves", "The space rangers", "Some socially malnourished nerds", "An innocent-looking pie", "A sentient reindeer", "Three scary Nuns", "You", "The internet", "The developer", "The 'IT' department", 'The missing body', "A horrible pun", "A terrible joke", "An inconsiderately discarded coffee table", "A hug-able tree", "A stray robot", "A sad little man", "An indescribable fellow", "A lonely planet", "A ravenous paperweight", "A basket of yummy things", "A figurative fig", "A very literal zebra", "An honest lawyer", "The last honest pizza", "A sarcastic dolphin", "An overconfident asparagus", "Leeroy Jenkins", "A meat dragon", "Chuck Norris", "Abe Lincoln"];
			var noun1 = noun[Math.floor(Math.random() * noun.length)];
			var noun2 = noun[Math.floor(Math.random() * noun.length)];
			while (noun1 == noun2) {
				noun2 = noun[Math.floor(Math.random() * noun.length)];
			}
			message += exclaim[Math.floor(Math.random() * exclaim.length)]+' ';
			message += msg[Math.floor(Math.random() * msg.length)]+': ';
			excuse += preface[Math.floor(Math.random() * preface.length)]+' ';
			excuse += noun1.toLowerCase()+' ';
			excuse += bridge[Math.floor(Math.random() * bridge.length)]+' ';
			excuse += verb[Math.floor(Math.random() * verb.length)]+' ';
			excuse += noun2.toLowerCase()+'!';
			excuse = excuse.replace('  ', '');
			return [message, excuse];
		}
	}
})();
document.onreadystatechange = function () {
  if (document.readyState === "interactive") {
    randoms.init();
  }
}